using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SelTumbuhanListManager : MonoBehaviour
{
    [Serializable]
    public struct SelObject
    {
        public GameObject obj;
        public Button button; 
    }

    public List<SelObject> sels = new List<SelObject>();
    
    private int selectedSel = 0;

    private void Awake()
    {
        Initialize();
    }

    private void OnEnable()
    {
        Reset();
    }

    public void Reset()
    {
        selectedSel = 0;

        foreach (SelObject sel in sels)
        {
            sel.obj.SetActive(false);
        }

        sels[selectedSel].obj.SetActive(true);
    }

    public void Initialize()
    {
        foreach (SelObject sel in sels)
        {
            sel.button.onClick.AddListener(() => Select(sels.IndexOf(sel)));
        }
    }

    public void Select(int id)
    {
        // disable sel sebelumnya
        sels[selectedSel].obj.SetActive(false);

        // set active selected sel
        selectedSel = id;
        sels[selectedSel].obj.SetActive(true);
    }
}
