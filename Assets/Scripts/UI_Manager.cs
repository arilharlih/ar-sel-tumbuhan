using UnityEngine;

public class UI_Manager : MonoBehaviour
{
    [Scene]
    public string menuScene;

    public void LoadMenuScene()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(menuScene);
        Screen.orientation = ScreenOrientation.Portrait;
    }
}
