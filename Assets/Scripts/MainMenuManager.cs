using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuManager : MonoBehaviour
{
    [Scene]
    public string arScene;
    
    public void LoadARScene()
    {
        SceneManager.LoadScene(arScene);
        Screen.orientation = ScreenOrientation.LandscapeLeft;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
